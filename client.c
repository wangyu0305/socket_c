/* ---------------------------------------------------------------------------
 Author Name:     ryan wang
 Client side
---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 60003

#include "defs.h"

int mySocket;

void initClientSocket();

void sendStr(char* str);

int printView(char* buffer);

int main() {
    char str[MAX_STR];
    char buffer[MAX_BUFF];
    int bytesRcv;
    initClientSocket();

/* get input from user and send to server */
    while (1) {
        printf("enter add (for add a new song)\nenter delete (for delete a song)\nenter view (for view song list from server side)\nenter quit (for quit the program)\n");
        printf("Please enter message: ");
        fgets(str, sizeof(str), stdin);
        str[strlen(str) - 1] = '\0';
        printf("%s\n", str);
        if(strcmp(str, "add") == 0 || strcmp(str, "ADD") == 0 || strcmp(str, "delete") == 0 || strcmp(str, "DELETE") == 0 || strcmp(str, "view") == 0 || strcmp(str, "VIEW") == 0 || strcmp(str, "quit") == 0 || strcmp(str, "QUIT") == 0){
            strcpy(buffer, str);
            send(mySocket, buffer, strlen(buffer), 0);
        }else{
            printf("input invaild, try again");
            continue;
        }
        if (strcmp(str, "add") == 0 || strcmp(str, "ADD") == 0) {
            sendStr("name");
            sendStr("artist");
            sendStr("album");
            sendStr("duration");
        }
        if (strcmp(str, "delete") == 0){
            sendStr("name");
        }
        if (strcmp(str, "view") == 0){
            printf(" --- song from server --- \n");
            while (1){
                buffer[0] = '\0';
                bytesRcv = recv(mySocket, buffer, sizeof(buffer), 0);
                buffer[bytesRcv] = '\0';
                if(bytesRcv == 0 || printView(buffer) == 0)
                    break;
            }
        }
        if (strcmp(str, "quit") == 0)
            break;
    }

/* close the socket */
    close(mySocket);

    return 0;
}

void initClientSocket() {
    struct sockaddr_in addr;
    int i;

/* create socket */
    mySocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (mySocket < 0) {
        printf("couldn't open socket\n");
        exit(-1);
    }

/* setup address */
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(SERVER_IP);
    addr.sin_port = htons((unsigned short) SERVER_PORT);

/* connect to server */

    i = connect(mySocket,
                (struct sockaddr *) &addr,
                sizeof(addr));
    if (i < 0) {
        printf("client could not connect!\n");
        exit(-1);
    }
}

// send string to server
void sendStr(char* str){
    char sin[MAX_STR];
    printf("enter %s for the song:", str);
    fgets(sin, sizeof(sin), stdin);
    sin[strlen(sin) - 1] = '\0';
    send(mySocket, sin, strlen(sin), 0);
}

// print result from server, separated by comma (,) end with symbol '0'
// in case server side have very very long string, send buffer will overflow, client side should continue to receive
int printView(char* buffer){
    int size = strlen(buffer);
    int cont;
    if(buffer[size-1] == '0')
        cont = 0;
    else
        cont = 1;
    buffer[size-1] = '\0';
    printf("name, artist, album, duration \n%s\n", buffer);
    return cont;
}
