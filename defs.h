
#define MAX_STR   32
#define MAX_BUFF  256

// song structure with string name, artist, album, duration (follow prof's instruction)

typedef struct song{
    char name[MAX_STR];
    char artist[MAX_STR];
    char album[MAX_STR];
    char duration[MAX_STR];
}songType;
