/* ---------------------------------------------------------------------------
 Author Name:     ryan wang
 server side
---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define MY_PORT 60003

#include "list.h"

void initServerSocket();

void waitForConnection();

void recvText(char *);

void recvNewSong(nodeList* list);

void deleteSong(nodeList* list);

void sendView(nodeList* list);

int myListenSocket, clientSocket;

void terminate();

int main() {
    char buffer[MAX_BUFF];
    nodeList list;
    list.head = NULL;
    list.tail = NULL;
    int i;
    initServerSocket();

    signal(SIGUSR1, terminate);

    while (myListenSocket != -1) {

/*  waiting for next client to connect  */
        waitForConnection();
/*  a client has connected  */

/* read messages from client and do something with it  */
        while (myListenSocket != -1) {
            recvText(buffer);
            printf("the client sent:  %s\n", buffer);
            if (strcmp(buffer, "quit") == 0) {
                break;
            }
            if (strcmp(buffer, "add") == 0) {
                recvNewSong(&list);
            }
            if (strcmp(buffer, "delete") == 0) {
                deleteSong(&list);
            }
            if (strcmp(buffer, "view") == 0) {
                sendView(&list);
            }
        }
/*  closing this client's connection  */
        close(clientSocket);
    }

/*  all done, no more clients will be connecting  */
    printf("end of program\n");
    cleanup(&list);
    //close(myListenSocket);

    return 0;
}

void initServerSocket() {
    struct sockaddr_in myAddr;
    int i;

/* create socket */
    myListenSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (myListenSocket < 0) {
        printf("couldn't open socket\n");
        exit(-1);
    }

/* setup my server address */
    memset(&myAddr, 0, sizeof(myAddr));
    myAddr.sin_family = AF_INET;
    myAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    myAddr.sin_port = htons((unsigned short) MY_PORT);

/* bind my listen socket */
    i = bind(myListenSocket,
             (struct sockaddr *) &myAddr,
             sizeof(myAddr));
    if (i < 0) {
        printf("couldn't bind socket\n");
        exit(-1);
    }

/* listen */
    i = listen(myListenSocket, 5);
    if (i < 0) {
        printf("couldn't listen\n");
        exit(-1);
    }
}

/* wait for connection request */
void waitForConnection() {
    struct sockaddr_in clientAddr;
    int addrSize;

    printf("waiting for connection... \n");

    clientSocket = accept(myListenSocket, (struct sockaddr *) &clientAddr, &addrSize);
    if (clientSocket < 0 && myListenSocket != -1) {
        printf("couldn't accept the connection\n");
        exit(-1);
    }
    if(myListenSocket != -1)
        printf("got one! \n");
}

// receive message from server side
void recvText(char *text) {
    char buff[MAX_BUFF];
    int bytesRcv;

    buff[0] = '\0';

    bytesRcv = recv(clientSocket, buff, sizeof(buff), 0);
    buff[bytesRcv] = '\0';
    strcpy(text, buff);
}

// receive add a new song information from client side
void recvNewSong(nodeList* list){
    char name[MAX_STR], artist[MAX_STR], album[MAX_STR], duration[MAX_STR];
    node* newNode = malloc(sizeof(node));
    recvText(name);
    recvText(artist);
    recvText(album);
    recvText(duration);
    initNode(newNode, name, artist, album, duration);
    addNode(list, newNode);
}

// delete a song, match the name from client side
void deleteSong(nodeList* list){
    char name[MAX_STR];
    recvText(name);
    removeSong(list, name);
}

// format linked list data, send to client side
void sendView(nodeList* list){
    node* head = list->head;
    char songlist[MAX_BUFF*2] = "";
    int empty = 0;
    while (head != NULL){
        formatNode(&head->song, songlist);
        if (head->next != NULL)
            strcat(songlist, "\n");
        else
            strcat(songlist, "0");
        head = head->next;
        empty++;
    }
    if (empty == 0){
        strcpy(songlist, " , , , ,0");
    }
    send(clientSocket, songlist, strlen(songlist), 0);
}

// receive user signal for terminate program
void terminate(){
    printf("user terminate\n");
    close(clientSocket);
    close(myListenSocket);
    myListenSocket = -1;
}
