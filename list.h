

#ifndef ASSIGNMENT_LIST_H
#define ASSIGNMENT_LIST_H

#include "defs.h"

// node structure, contain song data and a pointer point to next
typedef struct nodeTpye{
    songType song;
    struct nodeTpye* next;
}node;

// linked list contain two pointer head and tail
typedef struct listType{
    node* head;
    node* tail;
}nodeList;

void initNode(node* newSong, char* name, char* artist, char* album, char* duration);
void addNode(nodeList* list, node* n);
void removeSong(nodeList* list, char* name);
void cleanup(nodeList* list);
void formatNode(songType* song, char* formatted);

#endif //ASSIGNMENT_LIST_H
