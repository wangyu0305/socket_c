/*
  Author: wang, yu (ryan)
*/

this is a simple cutunes program, (similar to itunes language)
this program build with server side and client side, server side will always wait client side's information, operations
this program is build for store and manipulate user song with a linked list structure.

source file including: server.c
source file including: client.c
source file including: list.c
header file including: defs.h
header file including: list.h

To complie the program:
    make

To launching the server side program:
    ./server

To launching the client side program:
    ./client

To operating the program:
    you need to input some command (like add, delete, view, quit), you have to quit before log as other client side

    some operate example:

    enter add (for add a new song)
    enter delete (for delete a song)
    enter view (for view song list)
    enter quit (for quit the program)
    Please enter message: add
    add
    enter name for the song:a
    enter artist for the song:a
    enter album for the song:a
    enter duration for the song:1
    enter add (for add a new song)
    enter delete (for delete a song)
    enter view (for view song list)
    enter quit (for quit the program)
    Please enter message: add
    add
    enter name for the song:b
    enter artist for the song:b
    enter album for the song:b
    enter duration for the song:2
    enter add (for add a new song)
    enter delete (for delete a song)
    enter view (for view song list)
    enter quit (for quit the program)
    Please enter message: view
    view
     --- song from server ---
    name, artist, album, duration
    a, a, a, 1,
    b, b, b, 2,
    enter add (for add a new song)
    enter delete (for delete a song)
    enter view (for view song list)
    enter quit (for quit the program)
    Please enter message: delete
    delete
    enter name for the song:a
    enter add (for add a new song)
    enter delete (for delete a song)
    enter view (for view song list)
    enter quit (for quit the program)
    Please enter message: view
    view
     --- song from server ---
    name, artist, album, duration
    b, b, b, 2,

    Please enter message: quit
    quit



