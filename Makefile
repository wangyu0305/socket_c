DEBUG= -g
EXECS= server client 

all:	$(EXECS)

server:	server.c defs.h list.h
	gcc $(DEBUG) -o server server.c list.c

client:	client.c defs.h
	gcc $(DEBUG) -o client client.c

clean:
	rm -f $(EXECS)
