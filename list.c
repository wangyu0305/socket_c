/* ---------------------------------------------------------------------------
 Author Name:     ryan wang
 List
---------------------------------------------------------------------------*/

#include "list.h"
#include <string.h>
#include <stdlib.h>

// initialize node structure
void initNode(node* newSong, char* name, char* artist, char* album, char* duration){
    strcpy(newSong->song.name, name);
    strcpy(newSong->song.artist, artist);
    strcpy(newSong->song.album, album);
    strcpy(newSong->song.duration, duration);
}

// format the node structure to string
void formatNode(songType* song, char* formatted){
    strcat(formatted, song->name);
    strcat(formatted, ", ");
    strcat(formatted, song->artist);
    strcat(formatted, ", ");
    strcat(formatted, song->album);
    strcat(formatted, ", ");
    strcat(formatted, song->duration);
    strcat(formatted, ", ");
}

// add node to list
void addNode(nodeList* list, node* n){
    node* head = list->head;
    node* tail = list->tail;
    if(head == NULL){
        list->head = n;
        list->tail = n;
        return;
    }
    list->tail->next = n;
    list->tail = list->tail->next;
}

// remove node which has the same name from user input
void removeSong(nodeList* list, char* name){
    node* head = list->head;
    node* temp;
    if(head == NULL)
        return;
    if(strcmp(head->song.name, name) == 0){
        temp = head;
        list->head = head->next;
        free(temp);
        temp = NULL;
        return;
    }
    while (head->next != NULL){
        if(strcmp(head->next->song.name, name) == 0){
            temp = head->next;
            head->next = head->next->next;
            if(temp == list->tail)
                list->tail = head;
            free(temp);
            temp = NULL;
            break;
        }
        head = head->next;
    }
}

void cleanup(nodeList* list){
    node* head = list->head;
    node* temp;
    while (head != NULL){
        temp = head;
        head = head->next;
        free(temp);
        temp = NULL;
    }
}
